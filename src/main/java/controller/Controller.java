package controller;

import java.awt.Dimension;
import java.awt.Point;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import model.Model;
import model.ModelState;

public class Controller {
    ModelState state;
    BufferedImage picture;

    public Controller() {
        this.state = new ModelState();
        Model.state = this.state;
    }

    public void generatePicture(Dimension pictureSize) {
        this.state.imageHeight = (int) pictureSize.getHeight();
        this.state.imageWidth = (int) pictureSize.getWidth();

        this.picture = Model.generatePicture();
    }

    public BufferedImage getPicture() {
        return this.picture;
    }

    public double getRe(Point point) {
        return Model.mapPoint(point).getRe();
    }

    public double getIm(Point point) {
        return Model.mapPoint(point).getIm();
    }

    public void updateState(int x, int y) {
        this.state.updateState(x, y);
    }

    public void initialState() {
        this.state.initialState();
    }

    public void saveToFile(File file) throws IOException {
        ImageIO.write(this.picture, "png", file);
    }
}
