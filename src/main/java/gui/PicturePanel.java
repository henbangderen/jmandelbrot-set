package gui;

import controller.Controller;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import javax.swing.JPanel;

public class PicturePanel extends JPanel {
    Dimension pictureSize;
    Controller controller;
    BufferedImage picture;

    public PicturePanel(Dimension d, Controller c) {
        this.controller = c;
        this.pictureSize = d;
        setBackground(new Color(0, 153, 0));

        setPreferredSize(this.pictureSize);
        setMaximumSize(this.pictureSize);
    }

    private void doDrawing(Graphics g) {
        Graphics2D g2d = (Graphics2D) g;

        this.controller.generatePicture(this.pictureSize);
        this.picture = this.controller.getPicture();

        g.drawImage(this.picture, 0, 0, null);
    }

    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        doDrawing(g);
    }
}
