package gui;

import controller.Controller;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;
import javax.swing.UIManager;

public class MainFrame extends JFrame {
    private PicturePanel picturePanel;
    PicturePanelMouseAdapter picturePanelMouseAdapter;
    private JFileChooser fileChooser;
    private Controller controller;

    public MainFrame() {
        super("Mandelbrot");

        try {
            UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
        } catch (Exception e2) {
            System.out.println("Can not set look and feel.");
            e2.printStackTrace();
        }

        setLayout(new BorderLayout());

        this.controller = new Controller();
        this.picturePanel = new PicturePanel(new Dimension(1200, 800), this.controller);


        this.picturePanelMouseAdapter = new PicturePanelMouseAdapter();

        add(this.picturePanel, "Center");


        this.picturePanel.addMouseListener(this.picturePanelMouseAdapter);
        this.picturePanel.addMouseListener(new PicturePanelRepainter());
        this.picturePanel.addMouseMotionListener(this.picturePanelMouseAdapter);


        this.fileChooser = new JFileChooser();
        setJMenuBar(createMenuBar());

        setSize(new Dimension(1200, 850));
        setResizable(false);
        setDefaultCloseOperation(3);
        setVisible(true);
    }

    private class PicturePanelMouseAdapter extends MouseAdapter {
        private PicturePanelMouseAdapter() {
        }
    }

    private class PicturePanelRepainter extends MouseAdapter {
        private PicturePanelRepainter() {
        }

        public void mouseClicked(MouseEvent e) {
            int x = e.getX();
            int y = e.getY();


            MainFrame.this.controller.updateState(x, y);
            MainFrame.this.picturePanel.repaint();
        }
    }

    private JMenuBar createMenuBar() {
        JMenuBar menuBar = new JMenuBar();

        JMenu fileMenu = new JMenu("File");
        JMenuItem exportDataItem = new JMenuItem("Save picture");
        JMenuItem exitItem = new JMenuItem("Exit");

        fileMenu.add(exportDataItem);
        fileMenu.addSeparator();
        fileMenu.add(exitItem);


        JMenu windowMenu = new JMenu("Window");
        JMenuItem clearScreenItem = new JMenuItem("Forget");

        windowMenu.add(clearScreenItem);

        menuBar.add(fileMenu);
        menuBar.add(windowMenu);


        fileMenu.setMnemonic(70);
        exitItem.setAccelerator(KeyStroke.getKeyStroke(88, 2));

        exitItem.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                int action = JOptionPane.showConfirmDialog(MainFrame.this,
                        "Tired? Relax your eyes. Take fresh air.", "Confirm Exit", 2);

                if (action == 0) {
                    System.exit(0);
                }
            }
        });

        exportDataItem.setAccelerator(KeyStroke.getKeyStroke(83, 2));

        exportDataItem.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (MainFrame.this.fileChooser.showSaveDialog(MainFrame.this) == 0) {
                    try {
                        MainFrame.this.controller.generatePicture(new Dimension(2400, 1600));
                        MainFrame.this.controller
                                .saveToFile(MainFrame.this.fileChooser.getSelectedFile());
                    } catch (IOException e1) {
                        JOptionPane.showMessageDialog(MainFrame.this,
                                "Could not save data to file.", "ERROR", 0);
                        e1.printStackTrace();
                    }
                }
            }
        });

        clearScreenItem.setAccelerator(KeyStroke.getKeyStroke(70, 2));
        clearScreenItem.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                MainFrame.this.controller.initialState();
                MainFrame.this.picturePanel.repaint();
            }
        });
        return menuBar;
    }
}
