package model;

import java.awt.Point;

public class ModelState {
    public int imageHeight;
    public int imageWidth;
    public double minRe;
    public double maxRe;
    public double minIm;
    public double maxIm;
    static int maxIter;
    public int counter;

    public ModelState() {
        initialState();
    }

    public double getStep() {
        return (this.maxRe - this.minRe) / this.imageWidth;
    }

    public double getMaxIm() {
        return this.minIm + (this.maxRe - this.minRe) * this.imageHeight / this.imageWidth;
    }

    public void initialState() {
        this.minRe = -2.0D;
        this.maxRe = 1.0D;
        this.minIm = -1.0D;
        this.maxIm = getMaxIm();

        maxIter = 50;
        this.counter = 1;
    }

    public void updateState(int x, int y) {
        ComplexPoint cPoint = Model.mapPoint(new Point(x, y));
        double centerRe = cPoint.getRe();
        double centerIm = cPoint.getIm();

        double scale = 0.25D;
        double reDif = this.maxRe - this.minRe;
        double imDif = getMaxIm() - this.minIm;


        double newMaxRe = centerRe + 0.5D * reDif * scale;
        double newMinRe = centerRe - 0.5D * reDif * scale;

        double newMaxIm = centerIm + 0.5D * imDif * scale;
        double newMinIm = centerIm - 0.5D * imDif * scale;

        this.maxRe = newMaxRe * 1.0D;
        this.minRe = newMinRe * 1.0D;
        this.minIm = newMinIm * 1.0D;
        this.maxIm = newMaxIm * 1.0D;
        maxIter = 200;

        if (this.counter > 10) {
            maxIter = 300;
        }

        this.counter++;
        System.out.println(toString());
    }

    public String toString() {
        return "ModelState [imageHeight=" + this.imageHeight + ", imageWidth=" + this.imageWidth
                + ", minRe=" + this.minRe + ", maxRe=" + this.maxRe + ", minIm=" + this.minIm
                + ", maxIm=" + this.maxIm + ", getStep()=" + getStep() + ",counter=" + this.counter
                + "]";
    }
}
