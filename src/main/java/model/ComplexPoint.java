package model;

public class ComplexPoint {
    public double re;

    public ComplexPoint() {
        this.re = 0.0D;
        this.im = 0.0D;
    }

    public double im;

    public ComplexPoint(double re, double im) {
        this.re = re;
        this.im = im;
    }

    public String toString() {
        return "ComplexPoint [re=" + this.re + ", im=" + this.im + "]";
    }

    public double getRe() {
        return this.re;
    }

    public void setRe(double re) {
        this.re = re;
    }

    public double getIm() {
        return this.im;
    }

    public void setIm(double im) {
        this.im = im;
    }

    public double getModulus() {
        return Math.sqrt(this.re * this.re + this.im * this.im);
    }
}

