package model;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.image.BufferedImage;

public class Model {
    public static ModelState state;

    public Model(ModelState state) {
        Model.state = state;
    }

    public static ComplexPoint mapPoint(Point point) {
        ComplexPoint complexPoint = new ComplexPoint();
        complexPoint.setRe(state.minRe + point.getX() * state.getStep());
        complexPoint.setIm(state.getMaxIm() - point.getY() * state.getStep());

        return complexPoint;
    }

    public static int calcIterations(ComplexPoint cPoint, int maxIterations) {
        double testPointX = cPoint.getRe();
        double testPointY = cPoint.getIm();


        ComplexPoint orbit = new ComplexPoint();
        orbit.setRe(testPointX);
        orbit.setIm(testPointY);


        double x = orbit.getRe();
        double y = orbit.getIm();
        double temp = 0.0D;

        int i = 0;
        for (i = 0; i < maxIterations; i++) {
            if (4.0D < x * x + y * y) {
                return i;
            }

            temp = x;

            x = x * x - y * y + testPointX;
            y = 2.0D * temp * y + testPointY;
        }

        return i;
    }

    public static Color calcPixelColor(int iterations, int maxIterations) {
        int value2 = 255 * (maxIterations - iterations) / maxIterations;


        return new Color(value2, value2, value2);
    }

    public static BufferedImage generatePicture() {
        int imageWidth = state.imageWidth;
        int imageHeight = state.imageHeight;

        BufferedImage picture = new BufferedImage(imageWidth, imageHeight, 2);
        long startTime = System.currentTimeMillis();

        ComplexPoint point = null;
        Color color = null;
        Graphics2D g2 = (Graphics2D) picture.getGraphics();
        int iterations = 0;

        for (int i = 0; i < imageWidth; i++) {
            for (int j = 0; j < imageHeight; j++) {
                point = mapPoint(new Point(i, j));


                iterations = calcIterations(point, ModelState.maxIter);


                color = calcPixelColor(iterations, ModelState.maxIter);
                g2.setColor(color);
                g2.drawLine(i, j, i, j);
            }
        }

        long stopTime = System.currentTimeMillis();
        System.out.println("generatePicture executed in " + (stopTime - startTime) + " msecs");
        return picture;
    }
}

