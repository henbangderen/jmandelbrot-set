# jmandelbrot-set

Simple scalable visualization
https://en.wikipedia.org/wiki/Mandelbrot_set

## Prerequisites
* jdk >= 1.7
* mvn > 3.0

## Build
```	
mvn clean package
```
    
## Launch
   Double click on the **mandelbrot-set-1.0-SNAPSHOT-jar-with-dependencies.jar** in the **target** folder
   
   Or on a command line: 
```
java -jar target/mandelbrot-set-1.0-SNAPSHOT-jar-with-dependencies.jar
```

## Usage
Click the left mouse button on a place you want to see closer. 

Save a picture by the **Ctrl-s** shortcut.

## Screenshots
![](screenshots/initial-state.png)

![](screenshots/ornate-spiral.png)
	
![](screenshots/mad-snowmen.png)

### License
Copyright © 2015 Roman Levakin. Distributed under the Eclipse Public License 1.0
